// Array
export function addArrayElement(array: any[], element: any): any[];
export function mapArrayToObject(array: any[], key?: string): any;
export function removeArrayElement(array: any[], element: any, key?: string): any[];
export function updateArrayElement(array: any[], element: any, key?: string): any[];
export function updateArrayElements(array: any[], elements: any, key?: string): any[];

// Async
export function setDebounce(fn: Function, timeout?: number): Function;

// Date
export const MONTH_NAMES: string[];
export const WEEKDAY_NAMES: string[];
export function getDay(date: string): string;
export function getFirstDayOfMonth(date: string): string;
export function getLastDayOfMonth(date: string): string;
export function getLocaleDate(): string;
export function getMonth(date: string): string;
export function getMonthDates(date: string): string[];
export function getMonthDays(date: string): string[];
export function getMonthName(date: string): string;
export function getNumberOfDaysInMonth(y: number, m: number): number;
export function getYear(date: string): string;

// Env
export const isBrowser: boolean;
export const isNode: boolean;
export const isWebWorker: boolean;

// File Input
export function getFilesFromEvent(event: InputEvent): { files: File[], dirs: { tree: Object, files: File[] }[] };

// File
export function download(name: string, file: any): void;
export function getFileExtension(file: File): string;
export function getFileSizeFormatted(file: File, decimals?: number): string;
export function loadFile(file: any, options: { as: string }): Promise<string | ArrayBuffer | null>;

// Img
export function loadImage(url: string): Promise<HTMLImageElement>;

// Math
export function clamp(x: number, min?: number, max?: number): number;
export function round(x: number, p?: number): number;
export function deg(rad: number): number;
export function rad(deg: number): number;

// Misc
export function swap(x1: any, x2: any): void;
