# Javascript Commons

A set of common javascript utilities.

### Install

```
$ npm install @nrl/js-commons
```

### Development

 - **(1)** Clone this repository.

 - **(2)** Build: `npm run build:dev`

### Publish

First make sure your working tree is clean (every changes are committed). Then:

```
$ npm version [major | minor | patch]
$ npm publish
```

# License

The content of this repository is published under the terms of the [GNU LGPL v3](./LICENSE.txt).
