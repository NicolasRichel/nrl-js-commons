/**
 * @param {File} file
 * @returns {String}
 */
function getFileDirPath(file) {
  return file.webkitRelativePath?.split("/").slice(0, -1).join("/") ?? "";
}

/**
 * @param {FileSystemFileEntry} entry
 * @returns {Promise<File>}
 */
function getFileFromFileEntry(entry) {
  return new Promise((resolve, reject) => entry.file(resolve, reject));
}

/**
 * @param {FileSystemDirectoryEntry} entry
 * @returns {Promise<FileSystemEntry[]>}
 */
function getEntriesFromDirEntry(entry) {
  return new Promise((resolve, reject) =>
    entry.createReader().readEntries(resolve, reject)
  );
}

/**
 * @param {File[]} files
 * @returns {{ tree: Object, files: File[] }}
 */
function getTreeFromFiles(files) {
  const paths = Array.from(new Set(files.map(getFileDirPath)), dirPath =>
    dirPath.split("/")
  );

  const treeFromPath = (path, node, parent) => {
    if (path.length === 0) return [];

    const name = path.shift();
    if (!name) return [];

    const children = () => [treeFromPath(path)].flat();

    if (!node) return { name, children: children() };

    if (node.name === name) {
      if (node.children.length > 0) {
        node.children.forEach(child => treeFromPath(path, child, node));
      } else {
        node.children = children();
      }
    } else {
      parent?.children.push({ name, children: children() });
    }
    return node;
  };

  const tree = paths.reduce((node, path) => treeFromPath(path, node), null);

  return { tree, files };
}

/**
 * @param {FileSystemEntry} entry
 * @param {Object} tree
 * @param {File[]} files
 * @returns {{ tree: Object, files: File[] }}
 */
async function getTreeFromDirEntry(entry, tree = {}, files = []) {
  if (entry.isDirectory) {
    const childEntries = await getEntriesFromDirEntry(entry);
    const children = await Promise.all(
      childEntries.map(e => getTreeFromDirEntry(e, {}, files).then(res => res.tree))
    );
    Object.assign(tree, {
      name: entry.name,
      children: children.filter(child => child.name)
    });
  } else {
    files.push(await getFileFromFileEntry(entry));
  }
  return { tree, files };
}

/**
 * @param {InputEvent} event
 * @returns {{ files: File[], directories: { tree: Object, files: File[] }[] }}
 */
async function getFilesFromEvent(event) {
  /** @type {File[]} */
  let files = [];
  /** @type {{ tree: Object, files: File[] }[]} */
  let dirs = [];

  if (event.dataTransfer) {
    // Files from drag & drop
    const asyncFiles = [];
    const asyncDirs = [];
    for (const item of event.dataTransfer.items) {
      const entry = item.webkitGetAsEntry();
      if (entry.isDirectory) {
        asyncDirs.push(getTreeFromDirEntry(entry));
      } else {
        asyncFiles.push(getFileFromFileEntry(entry));
      }
    }
    files = await Promise.all(asyncFiles);
    dirs = await Promise.all(asyncDirs);
  } else {
    // Files from file input
    const inputFiles = Array.from(event.target.files);
    if (event.target.webkitdirectory) {
      const folder = getTreeFromFiles(inputFiles);
      dirs = folder.tree ? [folder] : [];
    } else {
      files = inputFiles;
    }
  }

  return { files, dirs };
}

export {
  getFilesFromEvent
};
