function swap(x1, x2) {
  const x = x1;
  x1 = x2;
  x2 = x;
}

export {
  swap
};
