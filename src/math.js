/**
 * Clamps a value between an upper and lower bound.
 *
 * @param {Number} x value to clamp
 * @param {Number} min minimum value (default: 0)
 * @param {Number} max maximum value (default: 1)
 * @returns clamped value
 */
function clamp(x, min = 0, max = 1) {
  return Math.min(Math.max(x, min), max);
}

/**
 * Round a number with a given precision (number of decimal places).
 *
 * @param {Number} x number to round
 * @param {Number} p wanted precision (default: 0)
 */
function round(x, p = 0) {
  const n = Math.pow(10, p);
  return Math.round( x * n ) / n;
}

/**
 * Convert an angle value from radians to degrees.
 *
 * @param {Number} rad angle in radians
 * @returns {Number} angle in degrees
 */
function deg(rad) {
  return rad * (180 / Math.PI);
}

/**
 * Convert an angle value from degrees to radians.
 *
 * @param {Number} rad angle in degrees
 * @returns {Number} angle in radians
 */
function rad(deg) {
  return deg * (Math.PI / 180);
}

export {
  clamp,
  round,
  deg,
  rad,
};
