function setDebounce(fn, timeout = 100) {
  let debounce;
  return function () {
    clearTimeout(debounce);
    debounce = setTimeout(() => fn(...arguments), timeout);
  };
}

function setThrottle(fn, delay = 100) {
  console.warn("[@nrl/js-commons] setThrottle: method not implemented yet."); // TODO
}

export {
  setDebounce,
  setThrottle
};
