/**
 * @param {String} url
 * @returns {Promise<HTMLImageElement>}
 */
async function loadImage(url) {
  const img = new Image();
  return new Promise((resolve, reject) => {
    img.onload = () => resolve(img);
    img.onerror = reject;
    img.src = url;
  });
}

export {
  loadImage
};
