/**
 * Download a file.
 *
 * @param {String} name file name
 * @param {any} file file data
 */
function download(name, file) {
  const link = document.createElement("a");
  link.style.display = "none";
  document.body.appendChild(link);
  link.href = file;
  link.download = name;
  link.click();
  link.remove();
}

/**
 * @param {File} file
 * @returns {String}
 */
function getFileExtension(file) {
  const parts = file.name.split(".");
  const extension = parts[parts.length - 1];
  return parts.length > 1 && extension ? `.${extension}` : "";
}

/**
 * @param {File} file
 * @param {Number} decimals
 * @returns {String}
 */
function getFileSizeFormatted(file, decimals = 0) {
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
  const { floor, log, pow } = Math;

  const b = parseFloat(file.size);

  if (!b || Number.isNaN(b)) return "0 Bytes";

  const i = b < 1 ? 0 : floor(log(b) / log(1024));
  const d = decimals < 0 || i === 0 ? 0 : decimals;

  return (b / pow(1024, i)).toFixed(d) + " " + sizes[i];
}

/**
 * 
 * @param {Blob} file file to load
 * @param {{ as: string }} options
 * @returns {Promise<string | ArrayBuffer | null>}
 */
function loadFile(file, { as } = {}) {
  const reader = new FileReader();
  return new Promise((resolve, reject) => {
    reader.onload = () => resolve(reader.result);
    reader.onabort = reject;
    reader.onerror = reject;
    switch (as) {
      case "text": reader.readAsText(file, "utf-8"); break;
      case "data-url": reader.readAsDataURL(file); break;
      default: reader.readAsArrayBuffer(file); break;
    }
  });
}

export {
  download,
  getFileExtension,
  getFileSizeFormatted,
  loadFile,
};
